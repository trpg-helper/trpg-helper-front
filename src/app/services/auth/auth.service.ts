import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';

import { AuthToken } from '../../models/auth/auth-token-model';
import { RegisterModel } from '../../models/auth/register-model';
import { EndpointService } from '../endpoint/endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private cachedRequests: Array<HttpRequest<any>> = [];
  private isLoggedIn = false;

  constructor(
    private _http: HttpClient,
    private _endpoint: EndpointService,
    private _router: Router
  ) { }

  getIsLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  getAccessToken(): string {
    return localStorage.getItem('access_token'); // TODO: ???
  }

  getRefreshToken(): string {
    return localStorage.getItem('refresh_token'); // TODO: ???
  }

  collectFailedRequests(request: HttpRequest<any>): void {
    this.cachedRequests.push(request);
  }

  retryFailedRequests(): void {

  }

  /* ------------------------------------------------------------------ */

  register(userAndPass: RegisterModel): Observable<any> {
    return this._http.post(this._endpoint.getAuthRegisterHref(), userAndPass)
    .pipe(
      map((data: AuthToken) => {
        localStorage.setItem('refresh_token', data.refresh_token);
        localStorage.setItem('access_token', data.access_token);
      })
    );
  }

  authenticate(userAndPass: RegisterModel): Observable<any> {
    return this._http.post(this._endpoint.getAuthLoginHref(), userAndPass)
      .pipe(
        map((token: AuthToken) => {
          localStorage.setItem('refresh_token', token.refresh_token);
          localStorage.setItem('access_token', token.access_token);
          this.isLoggedIn = true;
        })
      );
  }

  refresh(): Observable<string> {
    return this._http.post(this._endpoint.getAuthRefreshHref(), {})
      .pipe(
        map((token: AuthToken) => token.access_token)
      );
  }

  logout(): void {
    this._http.post(this._endpoint.getAuthLogoutHref(), {})
      .pipe(
        finalize(() => this._router.navigateByUrl('/'))
      );
  }
}
