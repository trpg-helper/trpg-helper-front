import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { EndpointService } from '../endpoint/endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(
    private _endpoint: EndpointService,
    private _http: HttpClient
  ) { }

  public getRooms(): Observable<any> {
    return this._http.get(this._endpoint.getRoomBaseHref());
  }

  public addRoom(): Observable<any> {
    return this._http.post(this._endpoint.getRoomBaseHref(), {});
  }
}
