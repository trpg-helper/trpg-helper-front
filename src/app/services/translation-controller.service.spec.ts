import { inject, TestBed } from '@angular/core/testing';

import { TranslationControllerService } from './translation-controller.service';

describe('TranslationControllerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TranslationControllerService]
    });
  });

  it('should be created', inject([TranslationControllerService], (service: TranslationControllerService) => {
    expect(service).toBeTruthy();
  }));
});
