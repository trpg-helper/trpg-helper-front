import { Injectable } from '@angular/core';

import { EndpointGroup } from '../../models/endpoint/endpoint-group';

@Injectable({
  providedIn: 'root'
})
export class EndpointService {
  private domain = 'http://127.0.0.1';
  private port = ':5000';

  private auth: EndpointGroup = {
    baseHref: '/auth',
    endpoints: {
      REGISTER: '/register',
      LOGIN: '/login',
      REFRESH: '/refresh',
      LOGOUT: '/logout'
    }
  };

  private rooms: EndpointGroup = {
    baseHref: '/rooms',
    endpoints: {
      DISABLE: '/disable',
      ENABLE: '/enable',
      JOIN: '/join',
      LEAVE: '/leave'
    }
  };

  constructor() { }

  public getDomain(): string {
    return this.domain;
  }

  public getPort(): string {
    return this.port;
  }

  public getBaseHref(): string {
    return this.getDomain() + this.getPort();
  }

  public getAuthRegisterHref(): string {
    return this.getBaseHref() + this.auth.baseHref + this.auth.endpoints.REGISTER;
  }

  public getAuthLoginHref(): string {
    return this.getBaseHref() + this.auth.baseHref + this.auth.endpoints.LOGIN;
  }

  public getAuthRefreshHref(): string {
    return this.getBaseHref() + this.auth.baseHref + this.auth.endpoints.REFRESH;
  }

  public getAuthLogoutHref(): string {
    return this.getBaseHref() + this.auth.baseHref + this.auth.endpoints.LOGOUT;
  }

  public getRoomBaseHref(): string {
    return this.getBaseHref() + this.rooms.baseHref;
  }

  public getRoomIdHref(id: number): string {
    return this.getBaseHref() + this.rooms.baseHref + '/' + id;
  }

  public getRoomDisableHref(id: number): string {
    return this.getBaseHref() + this.getRoomIdHref(id) + this.rooms.endpoints.DISABLE;
  }

  public getRoomEnableHref(id: number): string {
    return this.getBaseHref() + this.getRoomIdHref(id) + this.rooms.endpoints.ENABLE;
  }

  public getRoomJoinHref(id: number): string {
    return this.getBaseHref() + this.getRoomIdHref(id) + this.rooms.endpoints.JOIN;
  }

  public getRoomLeaveHref(id: number): string {
    return this.getBaseHref() + this.getRoomIdHref(id) + this.rooms.endpoints.LEAVE;
  }
}
