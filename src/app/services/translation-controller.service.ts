import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class TranslationControllerService {

  constructor(
    private _translate: TranslateService
  ) {
    _translate.setDefaultLang('en');
    _translate.use('en');
  }

  public use(language: string): void {
    this._translate.use(language);
  }

  public getDefault(): string {
    return this._translate.getDefaultLang();
  }

  public getUsing(): string {
    return this._translate.currentLang;
  }
}
