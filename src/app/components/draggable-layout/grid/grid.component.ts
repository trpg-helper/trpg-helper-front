import { AfterViewInit, Component, OnInit } from '@angular/core';

import { MuuriOptions } from '../../../models/layout/muuri-options';
import { DraggableLayout } from '../draggable-layout';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent extends DraggableLayout implements OnInit, AfterViewInit {
  protected SELECTOR = '';
  protected OPTIONS: MuuriOptions = {};

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initLayout();
  }

  protected eventListener(reloadGrid: boolean): void { }

  protected submit(): void { }
}
