import { EventEmitter, Output } from '@angular/core';
import Muuri from 'muuri';

import { Layout } from '../../models/layout/layout';
import { MuuriOptions } from '../../models/layout/muuri-options';

export abstract class DraggableLayout {
 protected grid: Muuri;
 protected abstract SELECTOR: string;
 protected abstract OPTIONS: MuuriOptions;

 @Output() event: EventEmitter<boolean> = new EventEmitter();

 protected initLayout(): void {
  this.grid = new Muuri(this.SELECTOR, this.OPTIONS);
 }

 protected orderLayout(layouts: Array<Layout>): Array<Layout> {
  return layouts.sort((l1, l2) => {
   if (l1.order < l2.order) {
     return -1;
   }
   if (l1.order > l2.order) {
     return 1;
   }
   return 0;
 });
 }

 protected abstract eventListener(reloadGrid: boolean): void;
 protected abstract submit(): void;
}
