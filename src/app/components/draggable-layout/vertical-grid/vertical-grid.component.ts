import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MuuriOptions } from '../../../models/layout/muuri-options';
import { DraggableLayout } from '../draggable-layout';

@Component({
  selector: 'app-vertical-grid',
  templateUrl: './vertical-grid.component.html',
  styleUrls: ['./vertical-grid.component.scss']
})
export class VerticalGridComponent extends DraggableLayout implements OnInit, AfterViewInit {
  protected SELECTOR = '.board-column-content';
  protected OPTIONS: MuuriOptions = {
    items: '.board-item',
    layoutDuration: 400,
    layoutEasing: 'ease',
    dragEnabled: true,
    dragSortInterval: 0,
    dragReleaseDuration: 400,
    dragReleaseEasing: 'ease'
  };

  @Input() identifier: string;
  @Input() formGroup: FormGroup;

  ngOnInit() {
    this.SELECTOR += this.identifier;
  }

  ngAfterViewInit() {
    this.initLayout();

    this.grid
      .on('dragReleaseEnd', () => {
        console.log('aaaa');
      });
  }

  getControlNames(): Array<string> {
    const controls: Array<string> = [];

    for (const property in this.formGroup.controls) {
      if (this.formGroup.controls.hasOwnProperty(property)) {
        controls.push(property);
      }
    }

    return controls;
  }

  protected eventListener(reloadGrid: boolean): void { }

  protected submit(): void { }
}
