import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Control, InputType } from '../../../models/form/control';
import { KanbanLayout } from '../../../models/layout/kanban-layout';
import { Layout } from '../../../models/layout/layout';
import { MuuriOptions } from '../../../models/layout/muuri-options';
import { DraggableLayout } from '../draggable-layout';

@Component({
  selector: 'app-character-sheet',
  templateUrl: './character-sheet.component.html',
  styleUrls: ['./character-sheet.component.scss']
})
export class CharacterSheetComponent extends DraggableLayout implements OnInit, AfterViewInit {
  protected SELECTOR = '.grid';
  protected OPTIONS: MuuriOptions = {};

  public testGroup = new FormGroup({
    text: new Control('').setEnrichment({
      type: InputType.TEXT,
      label: 'Text:'
    }),
    number: new Control('').setEnrichment({
      type: InputType.NUMBER,
      label: 'Number:'
    }),
    color: new Control('').setEnrichment({
      type: InputType.COLOR,
      label: 'Color:'
    }),
    range: new Control('').setEnrichment({
      type: InputType.RANGE,
      label: 'Range:',
      min: 0,
      max: 10,
      step: 1
    }),
    checkbox: new Control('').setEnrichment({
      type: InputType.CHECKBOX,
      options: [
        {
          text: '1',
          value: 1
        },
        {
          text: '2',
          value: 2
        },
        {
          text: '3',
          value: 3
        }
      ]
    })
  });

  public layouts: Array<Layout> = [
    {
      order: 1,
      name: 'ay lmao',
      kanbanLayout: {
        title: 'Kanban 1',
        identifier: 'kanbanTest1',
        formGroup: this.testGroup
      } as KanbanLayout,
      width: 30
    } as Layout,
    {
      order: 2,
      name: 'ay lmao',
      kanbanLayout: {
        title: 'Kanban 1',
        identifier: 'kanbanTest1',
        formGroup: this.testGroup
      } as KanbanLayout,
      width: 30
    } as Layout,
    {
      order: 3,
      name: 'ay lmao',
      kanbanLayout: {
        title: 'Kanban 1',
        identifier: 'kanbanTest1',
        formGroup: this.testGroup
      } as KanbanLayout,
      width: 30
    } as Layout
  ];

  ngOnInit() { }

  ngAfterViewInit() {
    this.initLayout();
  }

  protected eventListener(reloadGrid: boolean): void { }
  protected submit(): void { }
}
