import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { KanbanLayout } from '../../../models/layout/kanban-layout';
import { MuuriOptions } from '../../../models/layout/muuri-options';
import { DraggableLayout } from '../draggable-layout';

@Component({
  selector: 'app-kanban',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.scss']
})
export class KanbanComponent extends DraggableLayout implements OnInit, AfterViewInit {
  protected SELECTOR = '.board';
  protected OPTIONS: MuuriOptions = {
    layoutDuration: 400,
    layoutEasing: 'ease',
    dragEnabled: true,
    dragSortInterval: 0,
    dragStartPredicate: {
      handle: '.board-column-header'
    },
    dragReleaseDuration: 400,
    dragReleaseEasing: 'ease'
  };

  @Input() layout: KanbanLayout;

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initLayout();

    this.grid
      .on('dragReleaseEnd', () => {
        this.event.emit(true);
      });
  }

  protected eventListener(reloadGrid: boolean): void { }

  protected submit(): void {
    console.log(this.layout.formGroup.value);
  }
}
