import { Component, OnInit } from '@angular/core';

import { Room } from '../../models/room/room';
import { EndpointService } from '../../services/endpoint/endpoint.service';
import { RoomService } from '../../services/room/room.service';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {
  rooms: Array<Room>;
  error: any;
  loading = true;

  constructor(
    private _endpoint: EndpointService,
    private _room: RoomService
  ) { }

  ngOnInit() {
    this.getRooms();
  }

  public getRooms(): void {
    this.loading = true;
    this.error = null;

    this._room.getRooms()
      .subscribe((rooms: any) => this.rooms = rooms.rooms,
        error => {
          this.error = error;
        },
        () => this.loading = false);
  }

  public addRoom(): void {
    this.loading = true;
    this.error = null;

    this._room.addRoom()
      .subscribe((room: any) => {},
        error => this.error = error,
        () => {
          this.loading = false;
          this.getRooms();
        });
  }
}
