import { Component, Input, OnInit } from '@angular/core';

import { Room } from '../../../models/room/room';

@Component({
  selector: 'app-room-info',
  templateUrl: './room-info.component.html',
  styleUrls: ['./room-info.component.scss']
})
export class RoomInfoComponent implements OnInit {
  @Input()room: Room;

  constructor() { }

  ngOnInit() {
  }

}
