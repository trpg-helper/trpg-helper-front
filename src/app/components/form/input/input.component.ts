import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Control, InputType } from '../../../models/form/control';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  inputType = InputType;

  @Input() control: Control;
  @Input() name: string;

  ngOnInit() { }

  public getFormGroup(): FormGroup {
    return this.control['_parent'];
  }
}
