import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { RegisterModel } from '../../models/auth/register-model';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  @ViewChild('username') username: ElementRef;
  @ViewChild('password') password: ElementRef;

  public loginForm: FormGroup;
  public message = '';

  constructor(
    private _auth: AuthService,
    private _http: HttpClient,
    private _router: Router
  ) { }

  ngOnInit(): void {
    if (this._auth.getIsLoggedIn()) {
      this._router.navigateByUrl('/hub');
    }

    this.loginForm = new FormGroup({
      username: new FormControl('string', [
        Validators.required,
        Validators.minLength(4)
      ]),
      password: new FormControl('string', [
        Validators.required,
        Validators.minLength(4)
      ])
    });
  }

  register(): void {
    this._auth.register({
      username: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value
    } as RegisterModel)
      .subscribe(() => this._router.navigateByUrl('/hub'), (error: string) => console.log(error));
  }

  login(): void {
    this._auth.authenticate({
      username: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value
    } as RegisterModel)
      .subscribe(() => this._router.navigateByUrl('/hub'), (error: string) => this.message = error);
  }
}
