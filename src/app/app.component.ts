import { Component, OnInit } from '@angular/core';

import { AuthService } from './services/auth/auth.service';

import { TranslationControllerService } from './services/translation-controller.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    public _auth: AuthService
  ) { }

  ngOnInit(): void { }
}
