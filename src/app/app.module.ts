import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharacterSheetComponent } from './components/draggable-layout/character-sheet/character-sheet.component';
import { GridComponent } from './components/draggable-layout/grid/grid.component';
import { KanbanComponent } from './components/draggable-layout/kanban/kanban.component';
import { VerticalGridComponent } from './components/draggable-layout/vertical-grid/vertical-grid.component';
import { InputComponent } from './components/form/input/input.component';
import { HubComponent } from './components/hub/hub.component';
import { LandingComponent } from './components/landing/landing.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RoomInfoComponent } from './components/room/room-info/room-info.component';
import { RoomComponent } from './components/room/room.component';
import { JwtInterceptor } from './interceptors/auth/jwt.interceptor';
import { AuthService } from './services/auth/auth.service';
import { EndpointService } from './services/endpoint/endpoint.service';
import { RoomService } from './services/room/room.service';
import { TranslationControllerService } from './services/translation-controller.service';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
  }),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    LandingComponent,
    NotFoundComponent,
    CharacterSheetComponent,
    KanbanComponent,
    VerticalGridComponent,
    GridComponent,
    InputComponent,
    RoomComponent,
    RoomInfoComponent,
    HubComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    TranslationControllerService,
    EndpointService,
    AuthService,
    RoomService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
