import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CharacterSheetComponent } from './components/draggable-layout/character-sheet/character-sheet.component';
import { HubComponent } from './components/hub/hub.component';
import { LandingComponent } from './components/landing/landing.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'landing',
    pathMatch: 'full'
  },
  {
    path: 'landing',
    component: LandingComponent
  },
  {
    path: 'character-sheet',
    component: CharacterSheetComponent
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
