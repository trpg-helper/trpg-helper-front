import { FormGroup } from '@angular/forms';

import { KanbanLayout } from './kanban-layout';

export interface Layout {
 order: number;
 name: string;
 kanbanLayout: KanbanLayout;
 width?: number;
 customCSS?: string;
}
