import { FormGroup } from '@angular/forms';

import { Control } from '../form/control';

export interface KanbanLayout {
 identifier: string | number;
 title: string;
 formGroup: FormGroup;
}
