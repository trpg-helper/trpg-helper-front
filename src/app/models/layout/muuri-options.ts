export interface MuuriOptions {
 dragEnabled?: boolean;
 layoutOnInit?: boolean;
 layoutDuration?: number;
 layoutEasing?: string;
 dragSortInterval?: number;
 dragStartPredicate?: Object;
 dragReleaseDuration?: number;
 dragReleaseEasing?: string;
 items?: string;
 dragContainer?: HTMLElement;
}

interface DragStartPredicate {
 handle?: string | boolean;
 distance?: number;
 delay?: number;
}
