export interface Room {
 join_enabled: boolean;
 max_players: number;
 id: number;
 master_token: string;
 player_token: string;
}
