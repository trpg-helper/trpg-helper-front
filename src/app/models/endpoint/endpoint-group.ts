export interface EndpointGroup {
 baseHref: string;
 endpoints?: {
  [name: string]: string
 };
}
