import { Option } from './option';

export interface Optgroup {
 label: string;
 options: Array<Option>;
}
