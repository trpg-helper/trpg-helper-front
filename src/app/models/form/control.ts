import { FormControl, FormGroup } from '@angular/forms';

import { Optgroup } from './optgroup';
import { Option } from './option';

export interface FormControlEnrichment {
 type: InputType;
 label?: string;
 placeholder?: string;
 options?: Array<Option>;
 radioGroup?: string;
 min?: number | string;
 max?: number | string;
 step?: number;
 optgroups?: Array<Optgroup>;
}

export class Control extends FormControl {
 private enrichment: FormControlEnrichment;

 public setEnrichment(enrichment: FormControlEnrichment): Control {
  this.enrichment = enrichment;
  return this;
 }

 public getEnrichment(): FormControlEnrichment {
  return this.enrichment;
 }
}

export enum InputType {
 TEXT = 'text',
 NUMBER = 'number',
 COLOR = 'color',
 RANGE = 'range',
 CHECKBOX = 'checkbox',
 DATE = 'date',
 // FILE = 'file',
 PASSWORD = 'password',
 RADIO = 'radio',
 TIME = 'time',
 TEXTAREA = 'textarea',
 SELECT = 'select',
 OPTGROUP = 'optgroup'
}
