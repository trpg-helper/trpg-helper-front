import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, filter, finalize, switchMap, take } from 'rxjs/operators';

import { AuthService } from '../../services/auth/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private isRefreshingToken = false;
  private tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(
    private _auth: AuthService,
    private _router: Router
  ) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    const tokenToBeAdded = req.url.includes('/auth/refresh') ? this._auth.getRefreshToken() : this._auth.getAccessToken();

    return next.handle(this.addToken(req, tokenToBeAdded))
      .pipe(
        catchError(error => {
          if (error instanceof HttpErrorResponse) {
            switch ((<HttpErrorResponse>error).status) {
              case 400:
                return this.handle400Error(error);
              case 401:
                return this.handle401Error(req, next);
            }
          } else {
            return Observable.throw(error);
          }
        })
      );
  }

  addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({ setHeaders: { Authorization: 'Bearer ' + token } });
  }

  handle400Error(error): Observable<any> {
    if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
      return this.logoutUser();
    }

    return Observable.throw(error);
  }

  private handle401Error(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      this.tokenSubject.next(null);

      return this._auth.refresh()
        .pipe(
          switchMap((newToken: string) => {
            if (newToken) {
              this.tokenSubject.next(newToken);
              return next.handle(this.addToken(req, newToken));
            }

            return this.logoutUser();
          }),
          catchError(error => this.logoutUser()),
          finalize(() => this.isRefreshingToken = false)
        );
    } else {
      return this.tokenSubject
        .pipe(
          filter(token => token !== null),
          take(1),
          switchMap(token => next.handle(this.addToken(req, token)))
        );
    }
  }

  logoutUser(): Observable<any> {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    this._router.navigateByUrl('/');
    return of('logging out');
  }
}
